#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

typedef struct node //construire structuri
{
    float *value;
    int *timestamp;
    struct node *next;
    struct node *prev;
} node_t;

typedef struct list
{
    node_t *head;
    node_t *tail;
    int len;
} list_t;

node_t *init_node(int timestamp, float value) // initializeaza un nod din lista
{
    node_t *new_node = malloc(sizeof(node_t));

    new_node->value = malloc(sizeof(float));
    *new_node->value = value;

    new_node->timestamp = malloc(sizeof(int));
    *new_node->timestamp = timestamp;

    new_node->next = NULL;
    new_node->prev = NULL;

    return new_node;
}

list_t *init_list() // initializeaza o lista vida
{
    list_t *new_list = malloc(sizeof(list_t));

    new_list->head = NULL;
    new_list->tail = NULL;
    new_list->len = 0;
    return new_list;
}

void free_node(node_t *nod) // elibereaza memoria ocupata de un nod
{
    free(nod->value);
    free(nod->timestamp);
    free(nod);
}

void destroy_list(list_t *list) // elibereaza zona de memorie ocupata de o lista,daca lista nu e vida
{
    node_t *aux;

    /* Se sterg toate nodurile din lista,incepand de la coada */
    while (list->len > 0)
    {
        aux = list->tail;              // Se retine un pointer
        list->tail = list->tail->prev; // Se muta pointerul cozii
        free_node(aux);                // Se elibereaza memoria ocupata de nodul eliminat
        --list->len;                   // Se decrementeaza lungiea listei
    }
    // Se elibereaza memoria ocupata de structura listei
    free(list);
}

void print_list(list_t *list) // afiseaza lista primita ca parametru
{
    node_t *aux = list->head;
    printf("%d\n", list->len);
    while (aux != NULL)
    {
        printf("%d %.2lf\n", *aux->timestamp, *aux->value);
        aux = aux->next;
    }
}

int insert_node(list_t *list, int timestamp, float value, int position) //inserare nod in lista
{
    if (position < 0 || position > list->len) //daca pozitia nu este in intervalul [0, len]
        return -1;

    node_t *new_node = init_node(timestamp, value); //initializare nod

    if (list->head == NULL) //inserare intr-o lista vida
    {
        list->head = new_node;
        list->tail = list->head;
    }
    else if (position == 0) //inserare la inceputul unei liste
    {
        new_node->next = list->head;
        list->head->prev = new_node;
        list->head = new_node;
    }
    else if (position == list->len) //inserare la sfarsitul listei
    {
        new_node->prev = list->tail;
        list->tail->next = new_node;
        list->tail = new_node;
    }
    else //inserare intr-o pozitie intermediara
    {
        int i = 0;
        node_t *crt = list->head, *prev = NULL;
        while (i < position)
        {
            prev = crt;
            crt = crt->next;
            i++;
        }

        prev->next = new_node;
        new_node->prev = prev;
        crt->prev = new_node;
        new_node->next = crt;
    }

    list->len++; //incrementare lungime lista
    return 0;
}

list_t *remove_node(list_t *list, int position)
{
    if (position < 0 || position > list->len - 1)
    {
        printf("Position out of bounds\n");
        return list;
    }

    if (list->len == 0)
    {
        printf("The list is already empty\n");
        return list;
    }

    if (position == 0)
    {
        node_t *aux = list->head;
        list->head = list->head->next;
        list->head->prev = NULL;
        free_node(aux);
        list->len--;
        return list;
    }

    if (position == list->len - 1)
    {
        node_t *aux = list->tail;
        list->tail = list->tail->prev;
        list->tail->next = NULL;
        free_node(aux);
        list->len--;
        return list;
    }

    int i = 0;
    node_t *aux = list->head;
    for (; i < position; i++, aux = aux->next)
        ;
    aux->prev->next = aux->next;
    aux->next->prev = aux->prev;
    free_node(aux);
    list->len--;
    return list;
}
list_t *sortList(list_t *list) //sortare lista crescator
{
    node_t *p;
    node_t *s;
    float aux;
    if (list->head == NULL) //verificare daca lista e goala
    {
        return NULL;
    }
    else
    {
        for (p = list->head; p != NULL; p = p->next)
        {

            for (s = p->next; s != NULL; s = s->next)
            {
                if (*p->value > *s->value) //se va face interschimbarea celor 2 valoriin cazul in care valoarea nodulului precedent e mai mare decat valoarea nodului urmator
                {
                    aux = *p->value;
                    *p->value = *s->value;
                    *s->value = aux;
                }
            }
        }
    }
    return list;
}

float media_aritmetica(list_t *list, int k, node_t *nou) //calcului mediei aritmetice intr-o fereastra de 5 elemente
{
    float s = 0;
    float x_aritm;
    node_t *x;
    for (x = nou; x != NULL && k != 0; x = x->next) //parcurgerea a 5 noduri a listei de la nodul transmis
    {
        s += *x->value;
        k--;
    }
    x_aritm = s / 5; //media aritmetica
    return x_aritm;
}

float deviatia_standard(list_t *list, int k, node_t *nou, float m)
{
    float S = 0;
    node_t *x;
    for (x = nou; x != NULL && k != 0; x = x->next) //parcurgerea a 5 noduria listei de la nodul transmis
    {
        float r = (*x->value - m); //m reprezinta media aritmetica a celor 5 noduri parcurse de la nodul transmis nou
        S += r * r;
        k--;
    }
    return sqrt(S / 5.0); //calculul si transmiterea deviatiei
}

list_t *eliminare_exceptii(list_t *list, int k)
{
    float x_aritmetica;
    float deviatia;
    list_t *lista_noua = init_list(); //initializare lista noua
    node_t *nou = list->head;
    int i = 0;
    int pos = 0;
    int m = 0;
    for (node_t *x = nou; x != NULL && m < 2; x = x->next) //adaugarea primelor 2 noduri ale listei (ce nu sunt luate in calcul) in lista noua
    {
        insert_node(lista_noua, *x->timestamp, *x->value, pos);
        pos++;
        m++;
    }
    while (i < list->len - 4) //ultimele 4 noduri vor forma deja o fereastra cu nodul de dinaintea lor

    {
        float scadere, adunare;
        x_aritmetica = media_aritmetica(list, k, nou);            //calculul mediei aritmetice pe fereastra respectiva
        deviatia = deviatia_standard(list, k, nou, x_aritmetica); //calculul deviatiei pe fereastra respectiva

        node_t *p = nou->next->next; //nodul central al ferestrei

        scadere = x_aritmetica - deviatia;                //calculul capatului inferior al intervalului
        adunare = x_aritmetica + deviatia;                //calculul capatului superior al intervalului
        if (*p->value >= scadere && *p->value <= adunare) //daca valoarea nodului central se afla in interval atunci acest nod este adaugat in lista noua
        {
            insert_node(lista_noua, *p->timestamp, *p->value, pos); //inserare nod
            pos++;
        }
        nou = nou->next; //trecerea la urm nod de unde va incepe urmatoarea fereastra
        i++;
    }
    int r = 0;
    for (node_t *x = nou->next->next; x != NULL && r < 3; x = x->next) //adaugarea ultimelor 2 noduri ale listei (ce nu sunt luate in calcul) in lista noua
    {
        insert_node(lista_noua, *x->timestamp, *x->value, pos);
        pos++;
        r++;
    }
    destroy_list(list); //distrugerea listei vechi
    return lista_noua;  //returneaza lista noua obtinuta
}

float valoare_mediana(list_t *list, node_t *nou, int k)
{
    node_t *x;
    list_t *lista = init_list(); //initializare lista noua
    int pos = 0;
    for (x = nou; x != NULL && k != 0; x = x->next) //adaug nodurile din fereastra intr-o noua lista
    {
        insert_node(lista, *x->timestamp, *x->value, pos); //inserez nodurile
        pos++;
        k--;
    }
    lista = sortList(lista); //sortez lista
    node_t *new = lista->head;
    float m = *new->next->next->value; //retin valoarea nodului central din lista construita
    destroy_list(lista);               //distrug lista

    return m; //returnez valoarea obtinuta
}
list_t *filtrare_mediana(list_t *list, int k)
{
    int pos = 0, i = 0;
    node_t *nou = list->head;
    list_t *lista_noua = init_list(); //initializare lista noua
    while (i < list->len - 4)

    {
        node_t *p = nou->next->next;             //nodul central al ferestrei
        int g = *p->timestamp;                   //retin timestampul acestui nod
        float h = valoare_mediana(list, nou, 5); //primesc valoarea nodului din lista sortata
        insert_node(lista_noua, g, h, pos);      //inserez nodul cu timestampul si valoarea retinute in lista noua
        pos++;
        nou = nou->next;
        i++;
    }
    destroy_list(list); //distrug lista veche
    return lista_noua;  //returnez lista obtinuta
}

list_t *filtrare_media_aritmetica(list_t *list, int k)
{

    float x_aritmetica;
    node_t *nou = list->head;
    list_t *newlist = init_list(); //initializare lista
    int pos = 0, i = 0;

    while (i < list->len - 4)
    {
        node_t *p = nou->next->next;                   //nodul central al ferestrei
        int g = *p->timestamp;                         //retin timestamp-ul nodului central
        x_aritmetica = media_aritmetica(list, k, nou); //media aritmetica a nodurilor din fereastra respectiva
        insert_node(newlist, g, x_aritmetica, pos);    //inserez nodul in lista noua cu valoarea mediei aritmetice si timestampul retinut
        pos++;
        nou = nou->next;
        i++;
    }
    destroy_list(list); //distrugere lista veche
    return newlist;     //returnare lista noua
}
list_t *frecventa_in_timp(list_t *list, int k)
{
    node_t *nod = list->head;
    node_t *p;
    node_t *s;
    int i = 0;
    while (i < list->len - 1)
    {
        p = nod;
        s = p->next; //succesorul nodului p
        int r = *s->timestamp - *p->timestamp;
        if (r >= 100 && r <= 1000) //daca diferenta apartine de interval
        {
            *s->value = (*p->value + *s->value) / 2.0;             //modific valoarea nodului s cu media valorilor celor 2 noduri
            *s->timestamp = (*p->timestamp + *s->timestamp) / 2.0; //modific timestampul nodului s cu media timestampurilor celor 2 noduri
        }
        nod = nod->next;
        i++;
    }

    return list; //returnez lista
}
void statistici(list_t *list, float delta)
{
    printf("%f \n", delta);
    list = sortList(list); //sortez lista
    float z;

    if (delta == 2)
        z = *list->head->value;
    else
        z = *list->head->value - 1.0; //iau valoarea primului nod din lista (sortata crescator)
    float capatsuperior = z + delta;
    while (capatsuperior <= *list->tail->value || (*list->tail->value >= z && *list->tail->value <= capatsuperior))
    //delta mai mic decat  valoarea ultimului noddin lista sau daca valoarea ultimului nod apartine de intervalul respectiv
    {
        int nr = 0;
        for (node_t *x = list->head; x != NULL; x = x->next) //parcurg lsita
        {
            if (*x->value >= z && *x->value <= capatsuperior) //daca respectiv valoarea nodului x apartine de interval
            {
                nr++; //creste nr de elemente ce s-au gasit in intervalul
            }
        }
        if (nr > 0)                                             //daca s-au gasit elemente
            printf("[%-2.f,%-2.f] %d\n", z, capatsuperior, nr); //afisez intervalul si nr de elemente gasite
                                                                //noul interval devine
        z = capatsuperior;
        capatsuperior = z + delta;
    }
}

int main(int argc, char *argv[])
{
    int n, pos = 0, s;
    float value;
    scanf("%d", &n);            //citire nr de perechi
    list_t *list = init_list(); //initializare lista vida

    for (int i = 0; i < n; i++)
    {
        scanf("%d%f", &s, &value);        //citire perechi
        insert_node(list, s, value, pos); //adaugare nod in lista
        pos++;
    }
    int k = 0;
    for (int i = 1; i < argc; i++) //argumente din linia de comanda
    {
        if (strcmp(argv[i], "--e1") == 0) //daca primesc argumentul  e1  atunci apelez functia eliminare_exceptii
            list = eliminare_exceptii(list, 5);
        else if (strcmp(argv[i], "--e2") == 0)
            list = filtrare_mediana(list, 5);
        else if (strcmp(argv[i], "--e3") == 0)
            list = filtrare_media_aritmetica(list, 5);
        else if (strcmp(argv[i], "--u") == 0)
            list = frecventa_in_timp(list, 5);
        else if (strcmp(argv[i], "--st2") == 0)
        {
            statistici(list, 2);
            k++;
        }
        else if (strcmp(argv[i], "--st10") == 0)
        {
            statistici(list, 10);
            k++;
        }
    }

    if (k == 0)
        print_list(list);
    destroy_list(list); //distrugere lista
    return 0;
}
